package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.Collection;
import fr.uavignon.ceri.tp3.data.CollectionRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private CollectionRepository repository;
    private MutableLiveData<Collection> city;

    public DetailViewModel (Application application) {
        super(application);
        repository = CollectionRepository.get(application);
        city = new MutableLiveData<>();
    }

    public void setCity(long id) {
        repository.getCity(id);
        city = repository.getSelectedCity();
    }
    LiveData<City> getCity() {
        return city;
    }

    public void MettreAjour(City city) {
        repository.loadWeatherCity(city);
    }
}

