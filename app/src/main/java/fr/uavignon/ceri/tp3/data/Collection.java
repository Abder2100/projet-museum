package fr.uavignon.ceri.tp3.data;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "Collection", indices = {@Index(value = {"name", "description"},
        unique = true)})
public class Collection {

    public static final String TAG = Collection.class.getSimpleName();

    public static final long ADD_ID = -1;

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="_id")
    private long id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="technicalDetails")
    private String technicalDetails;

    @ColumnInfo(name="brand")
    private String brand;

    @ColumnInfo(name="categories")
    private String categories;

    @ColumnInfo(name="year")
    private Integer year;

    @ColumnInfo(name="timeFrame")
    private String timeFrame;

    @ColumnInfo(name="pictures")
    private String pictures;


    @Ignore
    public Collection(@NonNull String name, @NonNull String description) {
        this.name = name;
    }



    public Collection(@NonNull String name, String description, String technicalDetails, String brand, String categories, Integer year, String timeFrame,String pictures) {
        this.name = name;
        this.description = description;
        this.technicalDetails=technicalDetails;
        this.brand=brand;
        this.categories=categories;
        this.timeFrame=timeFrame;
        this.year=year;
        this.pictures=pictures;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }


    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }

    public String getCategories() {
        return categories;
    }

    public String getBrand() {
        return brand;
    }

    public Integer getYear() {
        return year;
    }

    public String getTimeFrame() {
        return timeFrame;
    }

    public String getPictures() {
        return pictures;
    }

    public void setTechnicalDetails(String technicalDetails) {
        this.technicalDetails = technicalDetails;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public void setTimeFrame(String timeFrame) {
        this.timeFrame = timeFrame;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }
}
