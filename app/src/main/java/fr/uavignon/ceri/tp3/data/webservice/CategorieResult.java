package fr.uavignon.ceri.tp3.data.webservice;

import fr.uavignon.ceri.tp3.data.Collection;

public class CategorieResult {
    public static void transferInfo(ItemResponse body,String position, Collection collection) {
        collection.setName(body.name);
        collection.setDescription(body.description);
        collection.setBrand(body.brand);
        collection.setYear(body.year);


        if(body.technicalDetails != null){
            String technicalDetails = "";
            for(int i = 0; i< body.technicalDetails.size(); i++)
                technicalDetails = technicalDetails + ", " + body.technicalDetails.get(i);
            collection.setTechnicalDetails(technicalDetails.substring(2));
        }

        if(body.categories != null){
            String categorie = "";
            for(int i = 0; i< body.categories.size(); i++)
                categorie = categorie + ", " + body.categories.get(i);
            collection.setCategories(categorie.substring(2));
        }

        if(body.timeFrame != null){
            String timeFrame = "";
            for(int i = 0; i< body.timeFrame.size(); i++)
                timeFrame = timeFrame + ", " + body.timeFrame.get(i);
            collection.setTimeFrame(timeFrame.substring(2));
        }
        if(body.pictures != null) {
            int iterator = 0;
            for (String images : body.pictures.keySet()) {
                if (iterator == 0){
                    collection.setPictures("https://demo-lia.univ-avignon.fr/cerimuseum/items/"+position+"/images/"+images);
                }
                iterator+=1;
            }
        }

        /*

        if(body.categories != null) {
            String categories = "";
            for (Iterator it = body.categories.iterator(); it.hasNext(); )
                categories = categories + ", " + it + "";
            collection.setCategories(categories);
        }
        if(body.categories != null) {
            String timeFrame = "";
            for (Iterator it = body.timeFrame.iterator(); it.hasNext(); )
                timeFrame = timeFrame + ", " + it + "";
            collection.setTimeFrame(timeFrame);

 */
        }




        //Log.d("this is the collection",collection.toString());

}