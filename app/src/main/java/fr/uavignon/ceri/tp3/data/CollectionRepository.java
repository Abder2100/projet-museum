package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;
import android.widget.TextView;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CollectionDao;
import fr.uavignon.ceri.tp3.data.database.CollectionRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.CategorieResult;
import fr.uavignon.ceri.tp3.data.webservice.ItemResponse;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.CollectionRoomDatabase.databaseWriteExecutor;

public class CollectionRepository {

    private static final String TAG = CollectionRepository.class.getSimpleName();

    private LiveData<List<Collection>> allcategories;
    private MutableLiveData<Collection> selectedCity;
    private MutableLiveData<ArrayList<Collection>> CollectionList = new MutableLiveData<>();
    private static OWMInterface api;
    private static TextView weatherData;
    private CollectionDao categoriesDao;
    Collection collection = new Collection(null,null);


    private static volatile CollectionRepository INSTANCE;

    public synchronized static CollectionRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new CollectionRepository(application);
        }

        return INSTANCE;
    }


    public CollectionRepository(Application application) {
        CollectionRoomDatabase db = CollectionRoomDatabase.getDatabase(application);
        categoriesDao = db.collectionDao();
        allcategories = categoriesDao.getAllCatalogues();
        selectedCity = new MutableLiveData<>();
    }


    public LiveData<List<Collection>> getAllcategories() {
        return allcategories;
    }

    public MutableLiveData<Collection> getSelectedCity() {
        return selectedCity;
    }




    public long insertCity(Collection newCatalogue) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return categoriesDao.insert(newCatalogue);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCatalogue);
        return res;
    }

    public int updateCity(Collection catalogue) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return categoriesDao.update(catalogue);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(catalogue);
        return res;
    }

    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            categoriesDao.deleteCatalogue(id);
        });
    }

    public void getCity(long id)  {
        Future<Collection> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return categoriesDao.getCatalogueById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    static Retrofit getForecast(){

        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        return retrofit;
    }

    public void loadCollection(){
        OWMInterface  api = CollectionRepository.getForecast().create(OWMInterface.class);
        Call<Map<String, ItemResponse>> call = api.getCollection();
        call.enqueue(new Callback<Map<String, ItemResponse>>(){
            @Override
            public void onResponse(Call<Map<String, ItemResponse>> call, Response<Map<String, ItemResponse>> response) {

                for (String position: response.body().keySet()) {
                    CategorieResult.transferInfo(response.body().get(position),position, collection);
                    insertCity(collection);
                }

                //Log.d("this is the collection",response.body().toString());
            }

            @Override
            public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {

            }
        });

    }


}
