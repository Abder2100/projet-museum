package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp3.data.Collection;

@Database(entities = {Collection.class}, version = 1, exportSchema = false)
public abstract class CollectionRoomDatabase extends RoomDatabase {

    private static final String TAG = CollectionRoomDatabase.class.getSimpleName();

    public abstract CollectionDao collectionDao();

    private static CollectionRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static CollectionRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CollectionRoomDatabase.class) {
                if (INSTANCE == null) {
                            INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    CollectionRoomDatabase.class,"Collection")
                                    .addCallback(sRoomDatabaseCallback)
                                    .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        CollectionDao dao = INSTANCE.collectionDao();
                        dao.deleteAll();


                        Log.d(TAG,"database populated");
                    });

                }
            };



}
