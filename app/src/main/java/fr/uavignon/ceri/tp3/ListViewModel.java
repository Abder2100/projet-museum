package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Collection;
import fr.uavignon.ceri.tp3.data.CollectionRepository;

public class ListViewModel extends AndroidViewModel {
    private CollectionRepository repository;
    private LiveData<List<Collection>> allCities;


    public ListViewModel (Application application) {
        super(application);
        repository = CollectionRepository.get(application);
        allCities = repository.getAllcategories();
    }

    LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public void deleteCity(long id) {
        repository.deleteCity(id);
    }


}
