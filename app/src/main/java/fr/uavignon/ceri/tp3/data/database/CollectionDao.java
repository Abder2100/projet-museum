package fr.uavignon.ceri.tp3.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Collection;

@Dao
public interface CollectionDao {


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Collection collection);

    @Query("DELETE FROM Collection")
    void deleteAll();

    @Query("SELECT * from Collection ORDER BY name ASC")
    LiveData<List<Collection>> getAllCatalogues();

    @Query("SELECT * from Collection ORDER BY year ASC")
    Collection getCollectablesOrderByDate();

    @Query("SELECT * from Collection ORDER BY name ASC")
    List<Collection> getSynchrAllCatalogues();

    @Query("DELETE FROM Collection WHERE _id = :id")

    void deleteCatalogue(double id);

    @Query("SELECT * FROM Collection WHERE _id = :id")
    Collection getCatalogueById(double id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Collection collection);
}
