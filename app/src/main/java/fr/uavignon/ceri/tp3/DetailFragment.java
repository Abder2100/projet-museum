package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp3.data.City;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textCollectionName, textDescription, textTechnDetails, textCategorie, textBrand, textTimeFram, textYear;
    private ImageView imgWeather;
    private ProgressBar progress;
    


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected city
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        long cityID = args.getCityNum();
        Log.d(TAG,"selected id="+cityID);
        viewModel.setCity(cityID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textCollectionName = getView().findViewById(R.id.nameCity);
        textDescription = getView().findViewById(R.id.editDescription);
        textTechnDetails = getView().findViewById(R.id.editDetailTech);
        textCategorie = getView().findViewById(R.id.editCollection);
        textBrand = getView().findViewById(R.id.editBrand);
        textTimeFram = getView().findViewById(R.id.editPeriodUtili);
        textYear = getView().findViewById(R.id.editAnneeUti);

        imgWeather = getView().findViewById(R.id.iconeCollection);

        progress = getView().findViewById(R.id.progress);

        getView().findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Interrogation à faire du service web",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                String cityname = textCollectionName.getText().toString().trim();
                String Country= textDescription.getText().toString().trim();
                Collection catalogue = new Collection(cityname,Country);
                viewModel.MettreAjour(catalogue);


            }
        });

        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getCity().observe(getViewLifecycleOwner(),
                city -> {
                    if (city != null) {
                        Log.d(TAG, "observing city view");

                        textCollectionName.setText(city.getName());
                        textDescription.setText(city.getDescription());

                        if (city.getTechnicalDetails() != null)
                            textTechnDetails.setText((CharSequence) city.getTechnicalDetails());
                        if (city.getCategories() != null)
                            textCategorie.setText((CharSequence) city.getCategories());
                        if (city.getBrand() != null)
                            textBrand.setText(city.getBrand());
                        if (city.getTimeFrame() != null)
                            textTimeFram.setText((CharSequence) city.getTimeFrame());
                        if (city.getYear() != null)
                            textYear.setText(city.getYear().toString());
                        if (city.getPictures() != null){
                            Glide.with(this)
                                    .load(city.getPictures())
                                    .into(imgWeather);
                        }

                        

                    }
                });



    }


}